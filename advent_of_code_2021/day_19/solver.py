from __future__ import annotations

from typing import Callable, Dict, List, Optional, Set, Tuple
from dataclasses import dataclass
from itertools import combinations
from pathlib import Path


@dataclass(frozen=True)
class Point3d:
    x: int
    y: int
    z: int

    def __sub__(self, other: Point3d) -> Vector3d:
        return Vector3d(self.x - other.x, self.y - other.y, self.z - other.z)

    def __add__(self, other: Vector3d) -> Point3d:
        return Point3d(self.x + other.x, self.y + other.y, self.z + other.z)


@dataclass(frozen=True)
class Vector3d:
    x: int
    y: int
    z: int

    @property
    def magnitude_squared(self) -> int:
        return self.x ** 2 + self.y ** 2 + self.z ** 2


class BeaconSet:
    def __init__(self, scanner_name: str, beacons: Set[Point3d]):
        self.scanner_name = scanner_name
        self.beacons = beacons
        self._edges: Optional[Dict[int, Tuple[Point3d, Point3d]]] = None
        self._edge_lengths: Optional[set[int]] = None

    @property
    def edges(self) -> Dict[int, Tuple[Point3d, Point3d]]:
        if self._edges is None:
            self._compute_edges()
        assert self._edges is not None
        return self._edges

    @property
    def edge_lengths(self) -> set[int]:  # Gets the length of all inter-beacon distances (actually distance squared)
        if self._edge_lengths is None:
            self._compute_edges()
        assert self._edge_lengths is not None
        return self._edge_lengths

    def resolve(self, rotation_func: Callable[[Point3d], Point3d], translation: Vector3d) -> ResolvedBeaconSet:
        resolved_beacons = set(rotation_func(beacon) + translation for beacon in self.beacons)
        scanner_location = Point3d(0, 0, 0) + translation
        resolved_beacon_set = ResolvedBeaconSet(self.scanner_name, resolved_beacons, scanner_location)
        return resolved_beacon_set

    def _compute_edges(self) -> None:
        self._edges = {}
        self._edge_lengths = set()
        non_unique_lengths = set()
        for b1, b2 in combinations(self.beacons, 2):
            length = (b1 - b2).magnitude_squared
            if length in self._edges:
                non_unique_lengths.add(length)
            self._edges[length] = (b1, b2)
            self._edge_lengths.add(length)
        for length in non_unique_lengths:
            del self._edges[length]


class ResolvedBeaconSet(BeaconSet):
    def __init__(self, scanner_name: str, beacons: Set[Point3d], scanner_location: Point3d):
        super().__init__(scanner_name, beacons)
        self.scanner_location = scanner_location


class Solver:
    def __init__(self, filename: str):
        self.input_beacon_sets = Solver._parse_file(filename)
        self._resolved_beacon_sets: Optional[List[ResolvedBeaconSet]] = None

    @staticmethod
    def _parse_file(filename: str) -> List[BeaconSet]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_sections = input_text.split("\n\n")

        input_beacon_sets = []
        for section in input_sections:
            section_lines = section.splitlines()
            scanner_name = section_lines[0].replace("-", "").strip()
            beacons = set()
            for line in section_lines[1:]:
                x, y, z = line.split(",")
                beacons.add(Point3d(int(x), int(y), int(z)))
            input_beacon_sets.append(BeaconSet(scanner_name, beacons))
        return input_beacon_sets

    def _run_solver(self) -> None:
        self._resolved_beacon_sets = [
            ResolvedBeaconSet(
                self.input_beacon_sets[0].scanner_name,
                self.input_beacon_sets[0].beacons,
                Point3d(0, 0, 0),
            )
        ]
        unresolved_beacon_sets = self.input_beacon_sets[1:]

        scanner_comparisons_done = set()
        while unresolved_beacon_sets:
            for unresolved_beacon_set in unresolved_beacon_sets:
                for resolved_beacon_set in self._resolved_beacon_sets:
                    comparison = frozenset((unresolved_beacon_set.scanner_name, resolved_beacon_set.scanner_name))
                    if comparison in scanner_comparisons_done:
                        continue  # Skip comparisons we've already tried
                    fixed_beacon_set = _try_tesselate(resolved_beacon_set, unresolved_beacon_set)
                    scanner_comparisons_done.add(comparison)
                    if fixed_beacon_set:
                        unresolved_beacon_sets.remove(unresolved_beacon_set)
                        self._resolved_beacon_sets.append(fixed_beacon_set)
                        break

    def part_one(self) -> int:
        if not self._resolved_beacon_sets:
            self._run_solver()
        assert self._resolved_beacon_sets is not None

        all_beacons = set()
        for beacon_set in self._resolved_beacon_sets:
            all_beacons.update(beacon_set.beacons)
        return len(all_beacons)

    def part_two(self) -> int:
        if not self._resolved_beacon_sets:
            self._run_solver()
        assert self._resolved_beacon_sets is not None

        max_manhattan_distance = 0
        for bs1, bs2 in combinations(self._resolved_beacon_sets, 2):
            diff = bs1.scanner_location - bs2.scanner_location
            manhattan_distance = abs(diff.x) + abs(diff.y) + abs(diff.z)
            if manhattan_distance > max_manhattan_distance:
                max_manhattan_distance = manhattan_distance
        return max_manhattan_distance


def _try_tesselate(
    reference_beacon_set: ResolvedBeaconSet, candidate_beacon_set: BeaconSet
) -> Optional[ResolvedBeaconSet]:
    # Check the edges between the two sets
    common_edges = reference_beacon_set.edge_lengths & candidate_beacon_set.edge_lengths
    if len(common_edges) < 66:
        return None

    # Find two edges with a common beacon
    reference_edges: List[Tuple[Point3d, Point3d]] = []
    candidate_edges: List[Tuple[Point3d, Point3d]] = []
    for edge in common_edges:
        if edge in reference_beacon_set.edges and edge in candidate_beacon_set.edges:
            reference_edge = reference_beacon_set.edges[edge]
            candidate_edge = candidate_beacon_set.edges[edge]
            if len(reference_edges) == 0:
                reference_edges.append(reference_edge)
                candidate_edges.append(candidate_edge)
            elif len(reference_edges) == 1:
                if reference_edge[0] in reference_edges[0] or reference_edge[1] in reference_edges[0]:
                    reference_edges.append(reference_edge)
                    candidate_edges.append(candidate_edge)
                    break
    else:
        raise RuntimeError("couldn't find two candidate edges to work with")

    # Resolve the edges into matching beacons #0 is the common node, #1 & #2 are the other nodes from the two edges
    reference_beacons: List[Point3d] = []
    candidate_beacons: List[Point3d] = []
    set_r1 = set(reference_edges[0])
    set_r2 = set(reference_edges[1])
    set_c1 = set(candidate_edges[0])
    set_c2 = set(candidate_edges[1])
    reference_beacons.extend(set_r1 & set_r2)
    reference_beacons.extend(set_r1 - set_r2)
    reference_beacons.extend(set_r2 - set_r1)
    candidate_beacons.extend(set_c1 & set_c2)
    candidate_beacons.extend(set_c1 - set_c2)
    candidate_beacons.extend(set_c2 - set_c1)

    # Now search for a rotation that brings these three beacons into alignment
    for rotation_func in _rotation_funcs:
        rotated_candidate_beacons = list(map(rotation_func, candidate_beacons))
        if (
            reference_beacons[0] - rotated_candidate_beacons[0]
            == reference_beacons[1] - rotated_candidate_beacons[1]
            == reference_beacons[2] - rotated_candidate_beacons[2]
        ):
            translation = reference_beacons[0] - rotated_candidate_beacons[0]
            return candidate_beacon_set.resolve(rotation_func, translation)
    else:
        raise RuntimeError("couldn't find a rotation that makes the two scanners tesselate")


_rotation_funcs = [
    # +z forward, rotate clockwise
    lambda p: Point3d(p.x, p.y, p.z),
    lambda p: Point3d(p.y, -p.x, p.z),
    lambda p: Point3d(-p.x, -p.y, p.z),
    lambda p: Point3d(-p.y, p.x, p.z),
    # +y forward, rotate clockwise
    lambda p: Point3d(p.x, -p.z, p.y),
    lambda p: Point3d(-p.z, -p.x, p.y),
    lambda p: Point3d(-p.x, p.z, p.y),
    lambda p: Point3d(p.z, p.x, p.y),
    # +x forward, rotate clockwise
    lambda p: Point3d(-p.z, p.y, p.x),
    lambda p: Point3d(p.y, p.z, p.x),
    lambda p: Point3d(p.z, -p.y, p.x),
    lambda p: Point3d(-p.y, -p.z, p.x),
    # -z forward, rotate clockwise
    lambda p: Point3d(-p.x, p.y, -p.z),
    lambda p: Point3d(p.y, p.x, -p.z),
    lambda p: Point3d(p.x, -p.y, -p.z),
    lambda p: Point3d(-p.y, -p.x, -p.z),
    # -y forward, rotate clockwise
    lambda p: Point3d(p.x, p.z, -p.y),
    lambda p: Point3d(p.z, -p.x, -p.y),
    lambda p: Point3d(-p.x, -p.z, -p.y),
    lambda p: Point3d(-p.z, p.x, -p.y),
    # -x forward, rotate clockwise
    lambda p: Point3d(p.z, p.y, -p.x),
    lambda p: Point3d(p.y, -p.z, -p.x),
    lambda p: Point3d(-p.z, -p.y, -p.x),
    lambda p: Point3d(-p.y, p.z, -p.x),
]
