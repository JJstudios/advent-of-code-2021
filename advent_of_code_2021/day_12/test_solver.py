import pytest

from .solver import Solver


@pytest.fixture(name="example1_solver")
def fixture_example1_solver() -> Solver:
    return Solver("input_example1")


@pytest.fixture(name="example2_solver")
def fixture_example2_solver() -> Solver:
    return Solver("input_example2")


@pytest.fixture(name="example3_solver")
def fixture_example3_solver() -> Solver:
    return Solver("input_example3")


def test_part_one(example1_solver: Solver, example2_solver: Solver, example3_solver: Solver) -> None:
    assert example1_solver.part_one() == 10
    assert example2_solver.part_one() == 19
    assert example3_solver.part_one() == 226


def test_part_two(example1_solver: Solver, example2_solver: Solver, example3_solver: Solver) -> None:
    assert example1_solver.part_two() == 36
    assert example2_solver.part_two() == 103
    assert example3_solver.part_two() == 3509
