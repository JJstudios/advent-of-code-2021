from typing import Dict, List
from collections import defaultdict
from copy import copy
from pathlib import Path


class Solver:
    def __init__(self, filename: str):
        self._input_cave_map = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> Dict[str, List[str]]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()

        input_cave_map: Dict[str, List[str]] = defaultdict(list)
        for line in input_lines:
            cave1, cave2 = line.split("-")
            if cave1 != "end" and cave2 != "start":
                input_cave_map[cave1].append(cave2)
            if cave2 != "end" and cave1 != "start":
                input_cave_map[cave2].append(cave1)
        return input_cave_map

    def part_one(self) -> int:
        all_routes: List[List[str]] = []
        _walk_cave_part_one(self._input_cave_map, "start", [], all_routes)
        return len(all_routes)

    def part_two(self) -> int:
        all_routes: List[List[str]] = []
        _walk_cave_part_two(self._input_cave_map, "start", [], all_routes)
        return len(all_routes)


def _walk_cave_part_one(
    cave_map: Dict[str, List[str]], cave_to_enter: str, current_route: List[str], all_routes: List[List[str]]
) -> None:
    # Enter cave
    current_route.append(cave_to_enter)

    if cave_to_enter == "end":
        # If this is the end, we've found a route
        all_routes.append(copy(current_route))
    else:
        # Visit each possible cave from here
        for neighbour in cave_map[cave_to_enter]:
            if neighbour.islower() and neighbour in current_route:
                # Do not revisit small caves
                continue
            else:
                _walk_cave_part_one(cave_map, neighbour, current_route, all_routes)

    # Exit this cave
    current_route.pop()


def _walk_cave_part_two(
    cave_map: Dict[str, List[str]], cave_to_enter: str, current_route: List[str], all_routes: List[List[str]]
) -> None:
    # Enter cave
    current_route.append(cave_to_enter)

    if cave_to_enter == "end":
        # If this is the end, we've found a route
        all_routes.append(copy(current_route))
    else:
        # Visit each possible cave from here
        for neighbour in cave_map[cave_to_enter]:
            if neighbour.islower() and neighbour in current_route:
                # Allowed to visit one small cave twice
                _walk_cave_part_one(cave_map, neighbour, current_route, all_routes)
            else:
                _walk_cave_part_two(cave_map, neighbour, current_route, all_routes)

    # Exit this cave
    current_route.pop()
