from typing import List, Tuple
from pathlib import Path


class Solver:
    def __init__(self, filename: str):
        self._input_algorithm, self._input_image = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> Tuple[List[bool], List[List[bool]]]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_section_algorithm, input_section_image = input_text.split("\n\n")

        input_algorithm = [c == "#" for c in input_section_algorithm]
        input_image = [[c == "#" for c in line] for line in input_section_image.splitlines()]
        return input_algorithm, input_image

    def part_one(self) -> int:
        image = self._input_image
        infinite_value = False
        for step in range(2):
            image, infinite_value = _enhance(self._input_algorithm, image, infinite_value)

        # Count the pixels
        assert not infinite_value
        return sum([sum(row) for row in image])

    def part_two(self) -> int:
        image = self._input_image
        infinite_value = False
        for step in range(50):
            image, infinite_value = _enhance(self._input_algorithm, image, infinite_value)

        # Count the pixels
        assert not infinite_value
        return sum([sum(row) for row in image])


def _enhance(algorithm: List[bool], image: List[List[bool]], infinite_value: bool) -> Tuple[List[List[bool]], bool]:
    image_size = len(image)
    new_image = [
        [_enhance_pixel(algorithm, row, col, image, infinite_value) for col in range(-1, image_size + 1)]
        for row in range(-1, image_size + 1)
    ]
    new_infinite_value = algorithm[-1] if infinite_value else algorithm[0]
    assert len(new_image) == image_size + 2
    assert len(new_image[0]) == image_size + 2
    return new_image, new_infinite_value


_rc_offset_table = [(-1, -1), (-1, 0), (-1, +1), (0, -1), (0, 0), (0, +1), (+1, -1), (+1, 0), (+1, +1)]


def _enhance_pixel(algorithm: List[bool], row: int, col: int, image: List[List[bool]], infinite_value: bool) -> bool:
    image_size = len(image)

    if 0 < row < image_size - 1 and 0 < col < image_size - 1:
        # Not near the edge
        pixel_lookup = 0
        for dr, dc in _rc_offset_table:
            pixel_lookup = (pixel_lookup * 2) + (1 if image[row + dr][col + dc] else 0)
    else:
        # Near the edge
        pixel_lookup = 0
        for dr, dc in _rc_offset_table:
            pixel_value = (
                image[row + dr][col + dc]
                if 0 <= row + dr < image_size and 0 <= col + dc < image_size
                else infinite_value
            )
            pixel_lookup = (pixel_lookup * 2) + (1 if pixel_value else 0)

    return algorithm[pixel_lookup]
