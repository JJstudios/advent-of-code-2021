import pytest

from .solver import SnailfishNumber, Solver


@pytest.fixture(name="example_solver")
def fixture_example_solver() -> Solver:
    return Solver("input_example")


def test_snailfishnumber_magnitude() -> None:
    assert SnailfishNumber("[9,1]").magnitude() == 29
    assert SnailfishNumber("[1,9]").magnitude() == 21
    assert SnailfishNumber("[[9,1],[1,9]]").magnitude() == 129
    assert SnailfishNumber("[[1,2],[[3,4],5]]").magnitude() == 143
    assert SnailfishNumber("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]").magnitude() == 1384
    assert SnailfishNumber("[[[[1,1],[2,2]],[3,3]],[4,4]]").magnitude() == 445
    assert SnailfishNumber("[[[[3,0],[5,3]],[4,4]],[5,5]]").magnitude() == 791
    assert SnailfishNumber("[[[[5,0],[7,4]],[5,5]],[6,6]]").magnitude() == 1137
    assert SnailfishNumber("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]").magnitude() == 3488


def test_snailfishnumber_explode() -> None:
    sfn = SnailfishNumber("[[[[[9,8],1],2],3],4]")
    assert sfn._try_explode()
    assert str(sfn) == "[[[[0,9],2],3],4]"

    sfn = SnailfishNumber("[7,[6,[5,[4,[3,2]]]]]")
    assert sfn._try_explode()
    assert str(sfn) == "[7,[6,[5,[7,0]]]]"

    sfn = SnailfishNumber("[[6,[5,[4,[3,2]]]],1]")
    assert sfn._try_explode()
    assert str(sfn) == "[[6,[5,[7,0]]],3]"

    sfn = SnailfishNumber("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]")
    assert sfn._try_explode()
    assert str(sfn) == "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"

    sfn = SnailfishNumber("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]")
    assert sfn._try_explode()
    assert str(sfn) == "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"


def test_snailfishnumber_split() -> None:
    sfn = SnailfishNumber("[0,0]")
    sfn._terms[3] = 10
    assert sfn._try_split()
    assert str(sfn) == "[0,[5,5]]"

    sfn = SnailfishNumber("[0,0]")
    sfn._terms[3] = 11
    assert sfn._try_split()
    assert str(sfn) == "[0,[5,6]]"

    sfn = SnailfishNumber("[0,0]")
    sfn._terms[3] = 12
    assert sfn._try_split()
    assert str(sfn) == "[0,[6,6]]"


def test_snailfishnumber_reduce() -> None:
    sfn = SnailfishNumber("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]")
    sfn.reduce()
    assert str(sfn) == "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"


def test_snailfishnumber_add() -> None:
    sfn1 = SnailfishNumber("[[[[4,3],4],4],[7,[[8,4],9]]]")
    sfn2 = SnailfishNumber("[1,1]")
    sfn = sfn1 + sfn2
    assert str(sfn) == "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"


def test_part_one(example_solver: Solver) -> None:
    assert example_solver.part_one() == 4140


def test_part_two(example_solver: Solver) -> None:
    assert example_solver.part_two() == 3993
