from __future__ import annotations

from typing import List, cast
from functools import reduce
from itertools import permutations
import math
import operator
from pathlib import Path


class SnailfishNumber:
    _grammar = ["[", ",", "]"]

    def __init__(self, input: str):
        if input[0] != "[" and input[-1] != "]":
            raise ValueError(f"{input} is not a valid SnailfishNumber")
        self._terms = [c if c in self._grammar else int(c) for c in input]

    def __str__(self) -> str:
        return "".join(map(str, self._terms))

    def magnitude(self) -> int:
        terms = self._terms.copy()
        while True:
            for n in range(len(terms) - 5, -1, -1):
                if terms[n] == "[" and terms[n + 2] == "," and terms[n + 4] == "]":
                    magnitude = 3 * cast(int, terms[n + 1]) + 2 * cast(int, terms[n + 3])
                    if n == 0:
                        return magnitude
                    terms = terms[:n] + [magnitude] + terms[n + 5 :]

    def __add__(self, other: SnailfishNumber) -> SnailfishNumber:
        new_sfn = SnailfishNumber(f"[{self},{other}]")
        new_sfn.reduce()
        return new_sfn

    def reduce(self) -> None:
        while True:
            if self._try_explode():
                continue
            if self._try_split():
                continue
            break

    def _try_explode(self) -> bool:
        depth = 0
        for n in range(0, len(self._terms)):
            if depth >= 4 and self._terms[n] == "[" and self._terms[n + 2] == "," and self._terms[n + 4] == "]":
                # Found a pair to explode!

                # Search for left neighbour
                for ln in range(n - 1, -1, -1):
                    if isinstance(self._terms[ln], int):
                        self._terms[ln] = cast(int, self._terms[ln]) + cast(int, self._terms[n + 1])
                        break

                # Search for right neighbour
                for rn in range(n + 5, len(self._terms)):
                    if isinstance(self._terms[rn], int):
                        self._terms[rn] = cast(int, self._terms[rn]) + cast(int, self._terms[n + 3])
                        break

                # Replace the exploding pair with zero
                self._terms = self._terms[:n] + [0] + self._terms[n + 5 :]
                return True

            if self._terms[n] == "[":
                depth += 1
            if self._terms[n] == "]":
                depth -= 1
        return False

    def _try_split(self) -> bool:
        for n in range(0, len(self._terms)):
            if isinstance(self._terms[n], int) and cast(int, self._terms[n]) > 9:
                # Found a number to split!
                self._terms = (
                    self._terms[:n]
                    + [
                        "[",
                        math.floor(cast(int, self._terms[n]) / 2),
                        ",",
                        math.ceil(cast(int, self._terms[n]) / 2),
                        "]",
                    ]
                    + self._terms[n + 1 :]
                )
                return True
        return False


class Solver:
    def __init__(self, filename: str):
        self._input_lines = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[str]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()
        return input_lines

    def part_one(self) -> int:
        all_sfns = [SnailfishNumber(line) for line in self._input_lines]
        return reduce(operator.add, all_sfns[1:], all_sfns[0]).magnitude()

    def part_two(self) -> int:
        all_sfns = [SnailfishNumber(line) for line in self._input_lines]
        return max((a + b).magnitude() for a, b in permutations(all_sfns, 2))
