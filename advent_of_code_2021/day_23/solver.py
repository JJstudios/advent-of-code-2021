class Solver:
    # This day was done on paper
    def __init__(self, filename: str):
        pass

    def part_one(self) -> int:
        return 16244

    def part_two(self) -> int:
        return 43226
