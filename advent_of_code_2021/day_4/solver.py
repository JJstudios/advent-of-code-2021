from typing import List, Optional, Tuple
from pathlib import Path


class Board:
    def __init__(self, board: str):
        self._board: List[List[int]] = [[int(n) for n in line.split()] for line in board.splitlines()]
        self._board_size = len(self._board)
        for row in self._board:
            if len(row) != self._board_size:
                raise ValueError("boards must be square")
        self.reset()

    def reset(self) -> None:
        self._dabs = [[False for n in row] for row in self._board]

    def dab(self, call: int) -> bool:
        for r in range(self._board_size):
            for c in range(self._board_size):
                if self._board[r][c] == call:
                    self._dabs[r][c] = True
                    return self._check_row(r) or self._check_col(c)
        return False

    def _check_row(self, r: int) -> bool:
        for c in range(self._board_size):
            if not self._dabs[r][c]:
                return False
        return True

    def _check_col(self, c: int) -> bool:
        for r in range(self._board_size):
            if not self._dabs[r][c]:
                return False
        return True

    def score(self) -> int:
        score = 0
        for r in range(self._board_size):
            for c in range(self._board_size):
                if not self._dabs[r][c]:
                    score += self._board[r][c]
        return score


class Solver:
    def __init__(self, filename: str):
        self._input_calls, self._input_boards = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> Tuple[List[int], List[Board]]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_section_calls, *input_sections_boards = input_text.split("\n\n")

        input_calls = [int(n) for n in input_section_calls.split(",")]
        input_boards = [Board(section) for section in input_sections_boards]
        return input_calls, input_boards

    def part_one(self) -> Optional[int]:
        for board in self._input_boards:
            board.reset()
        for call in self._input_calls:
            for board in self._input_boards:
                if board.dab(call):
                    return board.score() * call
        return None

    def part_two(self) -> Optional[int]:
        for board in self._input_boards:
            board.reset()
        boards = set(self._input_boards)
        for call in self._input_calls:
            for board in boards.copy():
                if board.dab(call):
                    if len(boards) == 1:
                        return board.score() * call
                    boards.remove(board)
        return None
