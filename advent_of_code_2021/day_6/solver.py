from typing import List
from pathlib import Path


class Solver:
    def __init__(self, filename: str):
        self._input_fishies = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[int]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        return [int(n) for n in input_text.split(",")]

    def part_one(self) -> int:
        fishies = self._input_fishies.copy()
        for day in range(80):
            for i in range(len(fishies)):
                if fishies[i] == 0:
                    fishies[i] = 6
                    fishies.append(8)
                else:
                    fishies[i] -= 1
        return len(fishies)

    def part_two(self) -> int:
        fishies = [0] * 9
        for n in self._input_fishies:
            fishies[n] += 1

        for day in range(256):
            num_spawning = fishies[0]
            for gen in range(8):
                fishies[gen] = fishies[gen + 1]
            fishies[6] += num_spawning
            fishies[8] = num_spawning
        return sum(fishies)
