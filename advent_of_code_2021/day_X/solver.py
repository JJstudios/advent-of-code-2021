from typing import List
from pathlib import Path


class Solver:
    def __init__(self, filename: str):
        self._input_data = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[int]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()
        return [int(line) for line in input_lines]

    def part_one(self) -> int:
        return 0

    def part_two(self) -> int:
        return 0
