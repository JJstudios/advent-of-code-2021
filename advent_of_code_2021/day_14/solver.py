from typing import Dict, Tuple
from collections import Counter, defaultdict
from itertools import pairwise
from pathlib import Path

from more_itertools import roundrobin


class Solver:
    def __init__(self, filename: str):
        self._input_template, self._input_rules = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> Tuple[str, Dict[str, str]]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_template, input_section_rules = input_text.split("\n\n")

        input_rules = {}
        for line in input_section_rules.splitlines():
            k, v = line.split(" -> ")
            input_rules[k] = v
        return input_template, input_rules

    def part_one(self) -> int:
        polymer = self._input_template
        for step in range(10):
            insertions = []
            for pair in ("".join(pr) for pr in pairwise(polymer)):
                insertions.append(self._input_rules[pair])
            polymer = "".join(roundrobin(polymer, insertions))

        counts = Counter(polymer).most_common()
        return counts[0][1] - counts[-1][1]

    def part_two(self) -> int:
        polymer_pairs: Dict[str, int] = defaultdict(int)
        for pair in ("".join(pr) for pr in pairwise(self._input_template)):
            polymer_pairs[pair] += 1

        for step in range(40):
            new_polymer_pairs: Dict[str, int] = defaultdict(int)
            for pair, count in polymer_pairs.items():
                insertion = self._input_rules[pair]
                new_polymer_pairs[pair[0] + insertion] += count
                new_polymer_pairs[insertion + pair[1]] += count
            polymer_pairs = new_polymer_pairs

        counts: Dict[str, int] = defaultdict(int)
        for pair, count in polymer_pairs.items():
            counts[pair[0]] += count
        counts[self._input_template[-1]] += 1

        return max(counts.values()) - min(counts.values())
