from typing import List, Optional
from functools import reduce
import operator
from pathlib import Path


class BitStream:
    def __init__(self, hex: str):
        self._binary_stream = "".join([f"{int(h,16):>04b}" for h in hex])
        self.position = 0

    def get_bits(self, num_bits: int) -> int:
        if self.position + num_bits > len(self._binary_stream):
            raise RuntimeError("overflow while accessing BitStream")
        chunk = self._binary_stream[self.position : self.position + num_bits]
        self.position += num_bits
        return int(chunk, 2)


class Packet:  # TODO: ABC
    def __init__(self, version: int, type_id: int):
        self.version = version
        self.type_id = type_id

    def evaluate(self) -> int:
        raise NotImplementedError


class LiteralPacket(Packet):
    def __init__(self, version: int, type_id: int, value: int):
        super().__init__(version, type_id)
        self.value = value

    def evaluate(self) -> int:
        return self.value


class OperatorPacket(Packet):
    def __init__(self, version: int, type_id: int, sub_packets: List[Packet]):
        super().__init__(version, type_id)
        self.sub_packets = sub_packets

    def evaluate(self) -> int:
        match self.type_id:
            case 0:  # sum
                return sum(p.evaluate() for p in self.sub_packets)
            case 1:  # product
                return reduce(operator.mul, (p.evaluate() for p in self.sub_packets), 1)
            case 2:  # minimum
                return min(p.evaluate() for p in self.sub_packets)
            case 3:  # maximum
                return max(p.evaluate() for p in self.sub_packets)
            case 5:  # greater than
                return 1 if self.sub_packets[0].evaluate() > self.sub_packets[1].evaluate() else 0
            case 6:  # less than
                return 1 if self.sub_packets[0].evaluate() < self.sub_packets[1].evaluate() else 0
            case 7:  # equal to
                return 1 if self.sub_packets[0].evaluate() == self.sub_packets[1].evaluate() else 0
            case _:
                raise NotImplementedError


class Solver:
    def __init__(self, filename: Optional[str] = None, input_hex: Optional[str] = None):
        if filename:
            self._input_hex = Solver._parse_file(filename)
        else:
            if input_hex is None:
                raise ValueError("must supply filename or input_hex")
            self._input_hex = input_hex

    @staticmethod
    def _parse_file(filename: str) -> str:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        return input_text.strip()

    def part_one(self) -> int:
        bit_stream = BitStream(self._input_hex)
        packet = parse_packet(bit_stream)
        return _total_version(packet)

    def part_two(self) -> int:
        bit_stream = BitStream(self._input_hex)
        packet = parse_packet(bit_stream)
        return packet.evaluate()


def parse_packet(bit_stream: BitStream) -> Packet:
    version = bit_stream.get_bits(3)
    type_id = bit_stream.get_bits(3)
    if type_id == 4:
        # Literal packet
        literal_value = 0
        while True:
            marker = bit_stream.get_bits(1)
            literal_value = literal_value << 4 | bit_stream.get_bits(4)
            if not marker:
                return LiteralPacket(version, type_id, literal_value)
    else:
        # Operator packet
        length_type_id = bit_stream.get_bits(1)
        if length_type_id:
            num_sub_packets = bit_stream.get_bits(11)
            sub_packets = [parse_packet(bit_stream) for _ in range(num_sub_packets)]
        else:
            length = bit_stream.get_bits(15)
            current_position = bit_stream.position
            sub_packets = []
            while bit_stream.position < current_position + length:
                sub_packets.append(parse_packet(bit_stream))
        return OperatorPacket(version, type_id, sub_packets)


def _total_version(packet: Packet) -> int:
    if isinstance(packet, OperatorPacket):
        return packet.version + sum(_total_version(sb) for sb in packet.sub_packets)
    else:
        return packet.version
