from .solver import Solver


def test_part_one() -> None:
    assert Solver(input_hex="D2FE28").part_one() == 6
    assert Solver(input_hex="38006F45291200").part_one() == 9
    assert Solver(input_hex="EE00D40C823060").part_one() == 14
    assert Solver(input_hex="8A004A801A8002F478").part_one() == 16
    assert Solver(input_hex="620080001611562C8802118E34").part_one() == 12
    assert Solver(input_hex="C0015000016115A2E0802F182340").part_one() == 23
    assert Solver(input_hex="A0016C880162017C3686B18A3D4780").part_one() == 31


def test_part_two() -> None:
    assert Solver(input_hex="D2FE28").part_two() == 2021
    assert Solver(input_hex="C200B40A82").part_two() == 3
    assert Solver(input_hex="04005AC33890").part_two() == 54
    assert Solver(input_hex="880086C3E88112").part_two() == 7
    assert Solver(input_hex="CE00C43D881120").part_two() == 9
    assert Solver(input_hex="D8005AC2A8F0").part_two() == 1
    assert Solver(input_hex="F600BC2D8F").part_two() == 0
    assert Solver(input_hex="9C005AC2F8F0").part_two() == 0
    assert Solver(input_hex="9C0141080250320F1802104A08").part_two() == 1
