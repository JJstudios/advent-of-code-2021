from typing import List, Optional
from pathlib import Path


class Diagnostic:
    def __init__(self, bits: str):
        self._bits = [bool(int(bit)) for bit in bits]

    def __len__(self) -> int:
        return len(self._bits)

    def __getitem__(self, key: int) -> bool:
        return self._bits[key]

    def __setitem__(self, key: int, value: bool) -> None:
        self._bits[key] = value

    def __int__(self) -> int:
        pattern = "".join(map(lambda b: "1" if b else "0", self._bits))
        return int(pattern, 2)


class Solver:
    def __init__(self, filename: str):
        self._input_diagnostics = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[Diagnostic]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()
        return [Diagnostic(line) for line in input_lines]

    def part_one(self) -> int:
        num_bits = len(self._input_diagnostics[0])
        gamma_rate = Diagnostic("0" * num_bits)
        epsilon_rate = Diagnostic("0" * num_bits)

        for bit_num in range(num_bits):
            bit = _most_common_bit(self._input_diagnostics, bit_num)
            if bit is None:
                raise ValueError(f"no most common bit in position {bit_num}")
            gamma_rate[bit_num] = bit
            epsilon_rate[bit_num] = not bit

        return int(gamma_rate) * int(epsilon_rate)

    def part_two(self) -> int:
        oxygen_generator_rating = _filter_by_bits(self._input_diagnostics, True)
        co2_scrubber_rating = _filter_by_bits(self._input_diagnostics, False)

        return int(oxygen_generator_rating) * int(co2_scrubber_rating)


def _most_common_bit(numbers: List[Diagnostic], bit_num: int) -> Optional[bool]:
    count = 0
    for number in numbers:
        if number[bit_num]:
            count += 1
        else:
            count -= 1
    if count == 0:
        return None
    return count > 0


def _filter_by_bits(numbers: List[Diagnostic], bit_criteria: bool) -> Diagnostic:
    numbers = numbers.copy()
    num_bits = len(numbers[0])
    bit_num = 0
    while len(numbers) > 1:
        if bit_num >= num_bits:
            raise RuntimeError("couldn't find a unique BitNum")
        bit = _most_common_bit(numbers, bit_num)
        if bit is None:
            bit = True
        if not bit_criteria:
            bit = not bit
        numbers = [n for n in numbers if n[bit_num] == bit]
        bit_num += 1
    return numbers[0]
