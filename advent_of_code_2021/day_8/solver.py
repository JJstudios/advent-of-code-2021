from typing import Any, List
from pathlib import Path

from more_itertools import bucket, partition


class Digit:
    def __init__(self, input: str):
        self.segments = set(c for c in input)

    def __len__(self) -> int:
        return len(self.segments)

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, self.__class__):
            return NotImplemented
        return self.segments == other.segments


class Display:
    def __init__(self, input: str):
        patterns, output = input.split(" | ")
        self.patterns = [Digit(digit) for digit in patterns.split()]
        self.output = [Digit(digit) for digit in output.split()]


class Solver:
    def __init__(self, filename: str):
        self._input_displays = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[Display]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()
        return [Display(line) for line in input_lines]

    def part_one(self) -> int:
        unique_lengths = [2, 3, 4, 7]
        answer = 0
        for display in self._input_displays:
            answer += sum(1 for digit in display.output if len(digit) in unique_lengths)
        return answer

    def part_two(self) -> int:
        return sum([_resolve_display(display) for display in self._input_displays])


def _resolve_display(display: Display) -> int:
    # Sort the patterns into lengths
    #   2: [#1], 3: [#7], 4: [#4], 5: [#2, #3, #5], 6: [#6, #9, #0], 7: [#8]
    patterns_by_length = bucket(display.patterns, len)

    # Digits which are unique by length
    digit_mapping = {
        1: next(patterns_by_length[2]),
        7: next(patterns_by_length[3]),
        4: next(patterns_by_length[4]),
        8: next(patterns_by_length[7]),
    }

    # Digits of length 6
    digits_not_9, digits_9 = partition(lambda d: d.segments > digit_mapping[4].segments, patterns_by_length[6])
    digit_mapping[9] = next(digits_9)
    digits_not_0, digits_0 = partition(lambda d: d.segments > digit_mapping[7].segments, digits_not_9)
    digit_mapping[0] = next(digits_0)
    digit_mapping[6] = next(digits_not_0)

    # Digits of length 5
    digits_not_3, digits_3 = partition(lambda d: d.segments > digit_mapping[7].segments, patterns_by_length[5])
    digit_mapping[3] = next(digits_3)
    digits_not_5, digits_5 = partition(lambda d: d.segments < digit_mapping[6].segments, digits_not_3)
    digit_mapping[5] = next(digits_5)
    digit_mapping[2] = next(digits_not_5)

    # Resolve the output
    output = 0
    for digit in display.output:
        digit_value = next(k for k, v in digit_mapping.items() if v == digit)
        output = 10 * output + digit_value
    return output
