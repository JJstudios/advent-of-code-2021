from __future__ import annotations

from typing import Iterator, List, Optional, Tuple
from dataclasses import dataclass
from pathlib import Path
import re


@dataclass
class Cuboid:
    min_x: int
    max_x: int
    min_y: int
    max_y: int
    min_z: int
    max_z: int

    @staticmethod
    def build(inputs: Iterator[int]) -> Cuboid:
        return Cuboid(next(inputs), next(inputs), next(inputs), next(inputs), next(inputs), next(inputs))

    @property
    def count(self) -> int:
        return (self.max_x + 1 - self.min_x) * (self.max_y + 1 - self.min_y) * (self.max_z + 1 - self.min_z)

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}"
            + f"({self.min_x}, {self.max_x}, {self.min_y}, {self.max_y}, {self.min_z}, {self.max_z})"
        )

    def __and__(self, other: Cuboid) -> Optional[Cuboid]:
        if (
            other.min_x > self.max_x
            or other.max_x < self.min_x
            or other.min_y > self.max_y
            or other.max_y < self.min_y
            or other.min_z > self.max_z
            or other.max_z < self.min_z
        ):
            return None

        return Cuboid(
            max(self.min_x, other.min_x),
            min(self.max_x, other.max_x),
            max(self.min_y, other.min_y),
            min(self.max_y, other.max_y),
            max(self.min_z, other.min_z),
            min(self.max_z, other.max_z),
        )


class Solver:
    def __init__(self, filename: str):
        self._input_reboot_steps = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[Tuple[bool, Cuboid]]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()

        input_reboot_steps = []
        regexp = re.compile(r"^(on|off) x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)")
        for line in input_lines:
            re_match = regexp.match(line)
            if not re_match:
                raise ValueError(f"failed to parse input line: {line}")
            on_off = re_match.group(1) == "on"
            cuboid = Cuboid.build(map(int, re_match.group(2, 3, 4, 5, 6, 7)))
            input_reboot_steps.append((on_off, cuboid))
        return input_reboot_steps

    def part_one(self) -> int:
        cube = [[[False for x in range(101)] for y in range(101)] for z in range(101)]

        initialisation_procedure_area = Cuboid(-50, 50, -50, 50, -50, 50)

        for on_off, cuboid in self._input_reboot_steps:
            if insecting_cuboid := cuboid & initialisation_procedure_area:
                for z in range(insecting_cuboid.min_z, insecting_cuboid.max_z + 1):
                    for y in range(insecting_cuboid.min_y, insecting_cuboid.max_y + 1):
                        for x in range(insecting_cuboid.min_x, insecting_cuboid.max_x + 1):
                            cube[z + 50][y + 50][x + 50] = on_off

        return sum([sum([sum(y) for y in z]) for z in cube])

    def part_two(self) -> int:
        cuboids_in: List[Cuboid] = []
        cuboids_out: List[Cuboid] = []

        for on_off, cuboid in self._input_reboot_steps:
            num_cuboids_in = len(cuboids_in)
            num_cuboids_out = len(cuboids_out)
            if on_off:
                cuboids_in.append(cuboid)
                for n in range(num_cuboids_in):
                    if intersecting_cuboid := cuboid & cuboids_in[n]:
                        cuboids_out.append(intersecting_cuboid)
                for n in range(num_cuboids_out):
                    if intersecting_cuboid := cuboid & cuboids_out[n]:
                        cuboids_in.append(intersecting_cuboid)
            else:
                for n in range(num_cuboids_in):
                    if intersecting_cuboid := cuboid & cuboids_in[n]:
                        cuboids_out.append(intersecting_cuboid)
                for n in range(num_cuboids_out):
                    if intersecting_cuboid := cuboid & cuboids_out[n]:
                        cuboids_in.append(intersecting_cuboid)

        return sum(map(lambda x: x.count, cuboids_in)) - sum(map(lambda x: x.count, cuboids_out))
