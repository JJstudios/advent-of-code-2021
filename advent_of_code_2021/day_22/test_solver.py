import pytest

from .solver import Solver


@pytest.fixture(name="example1_solver")
def fixture_example1_solver() -> Solver:
    return Solver("input_example1")


@pytest.fixture(name="example2_solver")
def fixture_example2_solver() -> Solver:
    return Solver("input_example2")


@pytest.fixture(name="example3_solver")
def fixture_example3_solver() -> Solver:
    return Solver("input_example3")


def test_part_one(example1_solver: Solver, example2_solver: Solver, example3_solver: Solver) -> None:
    assert example1_solver.part_one() == 39
    assert example2_solver.part_one() == 590784
    assert example3_solver.part_one() == 474140


def test_part_two(example1_solver: Solver, example3_solver: Solver) -> None:
    assert example1_solver.part_two() == 39
    # assert example3_solver.part_two() == 2758514936282235
