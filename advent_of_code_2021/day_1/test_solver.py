import pytest

from .solver import Solver


@pytest.fixture(name="example_solver")
def fixture_example_solver() -> Solver:
    return Solver("input_example")


def test_part_one(example_solver: Solver) -> None:
    assert example_solver.part_one() == 7


def test_part_two(example_solver: Solver) -> None:
    assert example_solver.part_two() == 5
