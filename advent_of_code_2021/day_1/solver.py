from typing import List
from itertools import pairwise
from pathlib import Path

from more_itertools import triplewise


class Solver:
    def __init__(self, filename: str):
        self._input_depths = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[int]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()
        return [int(line) for line in input_lines]

    def part_one(self) -> int:
        pairs = pairwise(self._input_depths)
        return sum(1 for x, y in pairs if x < y)

    def part_two(self) -> int:
        triple_sums = map(sum, triplewise(self._input_depths))  # type: map[int]
        pairs = pairwise(triple_sums)
        return sum(1 for x, y in pairs if x < y)
