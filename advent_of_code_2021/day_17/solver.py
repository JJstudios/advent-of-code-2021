from typing import Tuple, cast
from itertools import count
import math
from pathlib import Path
import re


class Solver:
    def __init__(self, filename: str):
        self._input_min_x, self._input_max_x, self._input_min_y, self._input_max_y = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> Tuple[int, int, int, int]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        re_match = re.match(r"^target area: x=(\d+)\.\.(\d+), y=(-\d+)\.\.(-\d+)", input_text)
        if not re_match:
            raise ValueError(f"failed to parse input: {filename}")
        return cast(Tuple[int, int, int, int], tuple(int(n) for n in re_match.groups()))

    def part_one(self) -> int:
        # To maximize the height, we will lob the probe so that it will be falling vertically through the target area:
        #   i.e. find an initial velocity for x (ivx) such that min_x <= sum(ivx..0) <= max_x
        ivxs = []
        for ivx in range(math.ceil(math.sqrt(2 * self._input_max_x))):
            if self._input_min_x <= _arithmetic_sum(ivx) <= self._input_max_x:
                ivxs.append(ivx)
        if not ivxs:
            raise RuntimeError("bad assumption - can't hit the target area with a lob")

        # The probe will fly in a parabolic arc back to the sea surface, and at that point it will have vy = -ivy-1
        #   and the next step down should hit as far into the target area as possible.
        #   Therefore, we want -ivy-1 = min_y => ivy = -(min_y + 1)
        ivy = -(self._input_min_y + 1)

        # This only holds true if the number of steps for x is less than or equal to the number of steps for y
        if ivy < max(ivxs):
            raise RuntimeError("bad assumption - target is too far away for a lob")

        # The top of this parabola is the height we will reach
        return _arithmetic_sum(ivy)

    def part_two(self) -> int:
        # Define the search area of velocities
        min_ivx = next(ivx for ivx in count() if _arithmetic_sum(ivx) >= self._input_min_x)
        max_ivx = self._input_max_x
        min_ivy = self._input_min_y
        max_ivy = -(self._input_min_y + 1)  # see above

        # Step through these and simulate
        hits = []
        for ivx in range(min_ivx, max_ivx + 1):
            for ivy in range(min_ivy, max_ivy + 1):
                # Simulate
                x = y = 0
                vx = ivx
                vy = ivy
                while True:
                    if x > self._input_max_x or y < self._input_min_y:
                        break
                    if self._input_min_x <= x <= self._input_max_x and self._input_min_y <= y <= self._input_max_y:
                        hits.append((ivx, ivy))
                        break
                    x += vx
                    y += vy
                    if vx > 0:
                        vx -= 1
                    vy -= 1

        return len(hits)


def _arithmetic_sum(n: int) -> int:
    return n * (n + 1) // 2
