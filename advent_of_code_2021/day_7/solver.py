from typing import List
from pathlib import Path


class Solver:
    def __init__(self, filename: str):
        self._input_crabs = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[int]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        return [int(n) for n in input_text.split(",")]

    def part_one(self) -> int:
        fuel = None
        central_position = 0
        while True:
            new_fuel = sum(map(lambda crab: abs(crab - central_position), self._input_crabs))
            if fuel and new_fuel > fuel:
                return fuel
            fuel = new_fuel
            central_position += 1

    def part_two(self) -> int:
        fuel = None
        central_position = 0
        while True:
            new_fuel = sum(map(lambda crab: _triangular_number(abs(crab - central_position)), self._input_crabs))
            if fuel and new_fuel > fuel:
                return fuel
            fuel = new_fuel
            central_position += 1


def _triangular_number(n: int) -> int:
    return (n * (n + 1)) // 2
