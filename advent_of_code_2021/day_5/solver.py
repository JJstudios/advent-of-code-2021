from typing import Iterable, List, Tuple
from pathlib import Path

import numpy as np
import numpy.typing as npt


class LineSegment:
    def __init__(self, x1: int, y1: int, x2: int, y2: int):
        x_diff = abs(x2 - x1)
        y_diff = abs(y2 - y1)
        if x_diff != 0 and y_diff != 0 and x_diff != y_diff:
            raise ValueError("line segment {x1},{y1} -> {x2},{y2} must be horizontal, vertical or diagonal")

        self._x1 = x1
        self._y1 = y1
        self._x2 = x2
        self._y2 = y2

    def is_horizontal_or_vertical(self) -> bool:
        return self._x1 == self._x2 or self._y1 == self._y2

    def get_extent(self) -> int:
        return max(self._x1, self._y1, self._x2, self._y2)

    def get_points(self) -> Iterable[Tuple[int, int]]:
        x_step = np.sign(self._x2 - self._x1)
        y_step = np.sign(self._y2 - self._y1)

        x = self._x1
        y = self._y1
        while True:
            yield (x, y)
            if x == self._x2 and y == self._y2:
                return
            x += x_step
            y += y_step


class Solver:
    def __init__(self, filename: str):
        self._line_segments = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[LineSegment]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()

        line_segments = []
        for line in input_lines:
            parts = line.replace(" -> ", ",").split(",")
            line_segments.append(LineSegment(int(parts[0]), int(parts[1]), int(parts[2]), int(parts[3])))
        return line_segments

    def part_one(self) -> int:
        field_size = max([ls.get_extent() for ls in self._line_segments]) + 1
        field = np.zeros((field_size, field_size), dtype=int)  # type: npt.NDArray[np.int_]

        for ls in self._line_segments:
            if ls.is_horizontal_or_vertical():
                for x, y in ls.get_points():
                    field[y, x] += 1

        return np.count_nonzero(field > 1)

    def part_two(self) -> int:
        field_size = max([ls.get_extent() for ls in self._line_segments]) + 1
        field = np.zeros((field_size, field_size), dtype=int)  # type: npt.NDArray[np.int_]

        for ls in self._line_segments:
            for x, y in ls.get_points():
                field[y, x] += 1

        return np.count_nonzero(field > 1)
