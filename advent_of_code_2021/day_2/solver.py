from typing import List, Tuple
from enum import Enum, auto
from pathlib import Path


class InstructionType(Enum):
    FORWARD = auto()
    DOWN = auto()
    UP = auto()


class Solver:
    def __init__(self, filename: str):
        self._input_instructions = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[Tuple[InstructionType, int]]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()

        instructions = []
        for line in input_lines:
            instruction_type, n = line.split()
            instructions.append((InstructionType[instruction_type.upper()], int(n)))
        return instructions

    def part_one(self) -> int:
        horizontal = 0
        depth = 0
        for instruction in self._input_instructions:
            match instruction:
                case (InstructionType.FORWARD, n):
                    horizontal += n
                case (InstructionType.DOWN, n):
                    depth += n
                case (InstructionType.UP, n):
                    depth -= n

        return horizontal * depth

    def part_two(self) -> int:
        aim = 0
        horizontal = 0
        depth = 0
        for instruction in self._input_instructions:
            match instruction:
                case (InstructionType.FORWARD, n):
                    horizontal += n
                    depth += aim * n
                case (InstructionType.DOWN, n):
                    aim += n
                case (InstructionType.UP, n):
                    aim -= n

        return horizontal * depth
