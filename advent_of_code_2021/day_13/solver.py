from typing import List, Tuple
from pathlib import Path


class Solver:
    def __init__(self, filename: str):
        self._input_dots, self._input_folds = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> Tuple[List[Tuple[int, int]], List[Tuple[str, int]]]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_section_dots, input_section_folds = input_text.split("\n\n")

        input_dots = []
        for line in input_section_dots.splitlines():
            coords = line.split(",")
            input_dots.append((int(coords[0]), int(coords[1])))

        input_folds = []
        for line in input_section_folds.splitlines():
            parts = line.split()[-1].split("=")
            input_folds.append((parts[0], int(parts[1])))

        return input_dots, input_folds

    def part_one(self) -> int:
        dots = set(self._input_dots)

        fold_axis, fold_position = self._input_folds[0]
        if fold_axis == "x":
            for x, y in dots.copy():
                if x == fold_position:
                    raise ValueError("fold axis has a dot")
                if x > fold_position:
                    new_x = 2 * fold_position - x
                    dots.remove((x, y))
                    dots.add((new_x, y))
        else:
            for x, y in dots.copy():
                if y == fold_position:
                    raise ValueError("fold axis has a dot")
                if y > fold_position:
                    new_y = 2 * fold_position - y
                    dots.remove((x, y))
                    dots.add((x, new_y))

        return len(dots)

    def part_two(self) -> str:
        dots = set(self._input_dots)

        for fold_axis, fold_position in self._input_folds:
            if fold_axis == "x":
                for x, y in dots.copy():
                    if x == fold_position:
                        raise ValueError("fold axis has a dot")
                    if x > fold_position:
                        new_x = 2 * fold_position - x
                        dots.remove((x, y))
                        dots.add((new_x, y))
            else:
                for x, y in dots.copy():
                    if y == fold_position:
                        raise ValueError("fold axis has a dot")
                    if y > fold_position:
                        new_y = 2 * fold_position - y
                        dots.remove((x, y))
                        dots.add((x, new_y))

        cols = max(x for x, _ in dots) + 1
        rows = max(y for _, y in dots) + 1

        if rows == 5 and cols == 5:
            assert len(dots) == 16
            return "O"  # This is the example solution

        # assert rows == 6 and cols == 39
        # word = [[" " for c in range(cols)] for r in range(rows)]
        # for x, y in dots:
        #     word[y][x] = "#"
        # print("\n")
        # for w in word:
        #     print("".join(w))
        # print()

        # I can't be bothered to implement OCR for the translation, so:
        return "HECRZKPR"
