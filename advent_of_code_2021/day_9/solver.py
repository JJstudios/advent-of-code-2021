from pathlib import Path

import numpy as np
import numpy.typing as npt


class Solver:
    def __init__(self, filename: str):
        self._input_heightmap = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> npt.NDArray[np.int_]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()

        input_heightmap = np.array([[int(n) for n in line] for line in input_lines])  # type: npt.NDArray[np.int_]
        return np.pad(input_heightmap, 1, constant_values=999)

    def part_one(self) -> int:
        sum_of_low_points = 0
        for y in range(1, self._input_heightmap.shape[0] - 1):
            for x in range(1, self._input_heightmap.shape[1] - 1):
                if (
                    self._input_heightmap[y - 1, x] > self._input_heightmap[y, x]
                    and self._input_heightmap[y, x + 1] > self._input_heightmap[y, x]
                    and self._input_heightmap[y + 1, x] > self._input_heightmap[y, x]
                    and self._input_heightmap[y, x - 1] > self._input_heightmap[y, x]
                ):
                    sum_of_low_points += self._input_heightmap[y, x] + 1

        return sum_of_low_points

    def part_two(self) -> int:
        heightmap = np.copy(self._input_heightmap)  # type: npt.NDArray[np.int_]
        basin_sizes = []

        for y in range(1, self._input_heightmap.shape[0] - 1):
            for x in range(1, self._input_heightmap.shape[1] - 1):
                if heightmap[y, x] < 9:
                    # Found a new basin
                    basin_size = _flood_fill(heightmap, x, y)
                    basin_sizes.append(basin_size)

        basin_sizes.sort()
        return basin_sizes[-1] * basin_sizes[-2] * basin_sizes[-3]


def _flood_fill(heightmap: npt.NDArray[np.int_], x: int, y: int) -> int:
    heightmap[y, x] = 99
    basin_size = 1
    if heightmap[y - 1, x] < 9:
        basin_size += _flood_fill(heightmap, x, y - 1)
    if heightmap[y, x + 1] < 9:
        basin_size += _flood_fill(heightmap, x + 1, y)
    if heightmap[y + 1, x] < 9:
        basin_size += _flood_fill(heightmap, x, y + 1)
    if heightmap[y, x - 1] < 9:
        basin_size += _flood_fill(heightmap, x - 1, y)
    return basin_size
