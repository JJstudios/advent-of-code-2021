from typing import Any
from argparse import ArgumentParser
from importlib import import_module

from timer import timer  # type: ignore


def main() -> None:
    parser = ArgumentParser()
    parser.add_argument("days", metavar="N", nargs="*", type=int, help="Run a specific day or days")
    parser.add_argument("--all", "-a", action="store_true", help="Run all days")
    args = parser.parse_args()

    if args.all:
        for day in range(1, 26):
            RunDay(day)
        exit(0)

    if not args.days:
        parser.print_help()
        exit(1)

    for day in args.days:
        RunDay(day)


def RunDay(day_num: int) -> None:
    try:
        module = import_module(f"day_{day_num}")
        solver = getattr(module, "Solver")("input")
        print(f"\nRunning \033[37;1mday {day_num}\033[0m:")
        RunSolverPart(solver, 1)
        RunSolverPart(solver, 2)
    except ModuleNotFoundError:
        pass


def RunSolverPart(solver: Any, part_num: int) -> None:  # TODO: This 'Any' is bad
    print(f"\t\tSolving part {part_num} ...", end="", flush=True)
    if part_num == 1:
        solver_method = solver.part_one
    elif part_num == 2:
        solver_method = solver.part_two
    else:
        raise ValueError(f"Bad value for {part_num=}")

    with timer() as t:
        solution = solver_method()

    if t.elapse < 10:
        elapsed = f"{1000*t.elapse:>4.0f} ms"  # xxxx ms
    else:
        elapsed = f"{t.elapse:>4.0f}  s"  # xxxx  s
    if t.elapse > 15:
        elapsed = "\033[33m" + elapsed + "\033[0m"
    print(f"\033[3D(took {elapsed}): \033[32m{solution}\033[0m")


if __name__ == "__main__":
    main()
