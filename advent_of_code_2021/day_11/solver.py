from typing import List
import copy
import itertools
from pathlib import Path


class Octopus:
    def __init__(self, energy_level: int):
        self._energy_level = energy_level
        self._should_flash = False

    def increment(self) -> None:
        self._energy_level += 1
        if self._energy_level == 10:
            self._should_flash = True

    @property
    def should_flash(self) -> bool:
        return self._should_flash

    def flash(self) -> None:
        assert self._should_flash
        self._should_flash = False

    def reset_flashers(self) -> None:
        if self._energy_level > 9:
            self._energy_level = 0


class Solver:
    def __init__(self, filename: str):
        self._input_octopuses = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[List[Octopus]]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()

        input_octopuses = []
        for line in input_lines:
            input_octopuses.append([Octopus(int(n)) for n in line])
        return input_octopuses

    def part_one(self) -> int:
        octopuses = copy.deepcopy(self._input_octopuses)

        total_num_flashes = 0
        for _ in range(100):
            num_flashes = _run_step(octopuses)
            total_num_flashes += num_flashes
        return total_num_flashes

    def part_two(self) -> int:
        octopuses = copy.deepcopy(self._input_octopuses)

        for step in itertools.count(start=1):
            num_flashes = _run_step(octopuses)
            if num_flashes == 100:
                return step
        raise NotImplementedError


def _run_step(octopuses: List[List[Octopus]]) -> int:
    for r in range(10):
        for c in range(10):
            octopuses[r][c].increment()

    def increment_octopus_at(r: int, c: int) -> None:
        if r >= 0 and r < 10 and c >= 0 and c < 10:
            octopuses[r][c].increment()

    num_flashes = 0
    while True:
        num_flashes_this_loop = 0
        for r in range(10):
            for c in range(10):
                if octopuses[r][c].should_flash:
                    num_flashes_this_loop += 1
                    octopuses[r][c].flash()
                    increment_octopus_at(r - 1, c - 1)
                    increment_octopus_at(r - 1, c)
                    increment_octopus_at(r - 1, c + 1)
                    increment_octopus_at(r, c - 1)
                    increment_octopus_at(r, c + 1)
                    increment_octopus_at(r + 1, c - 1)
                    increment_octopus_at(r + 1, c)
                    increment_octopus_at(r + 1, c + 1)
        if num_flashes_this_loop == 0:
            break
        num_flashes += num_flashes_this_loop

    for r in range(10):
        for c in range(10):
            octopuses[r][c].reset_flashers()

    return num_flashes
