from typing import List
from pathlib import Path


class Solver:
    def __init__(self, filename: str):
        self._input_lines = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[str]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()
        return input_lines

    def part_one(self) -> int:
        scores = [_check_syntax(line) for line in self._input_lines]
        return -sum(score for score in scores if score < 0)

    def part_two(self) -> int:
        scores = [_check_syntax(line) for line in self._input_lines]
        scores = [score for score in scores if score >= 0]
        scores.sort()
        return scores[len(scores) // 2]


_opening_brackets = ["(", "[", "{", "<"]
_closing_brackets = [")", "]", "}", ">"]
_brackets_mapping = dict(zip(_closing_brackets, _opening_brackets)) | dict(zip(_opening_brackets, _closing_brackets))


def _check_syntax(line: str) -> int:
    syntax_error_scores_mapping = {")": 3, "]": 57, "}": 1197, ">": 25137}
    autocomplete_scores_mapping = {")": 1, "]": 2, "}": 3, ">": 4}

    stack: List[str] = []
    for bracket in line:
        if bracket in _closing_brackets:
            if stack[-1] == _brackets_mapping[bracket]:
                stack.pop()
            else:
                return -syntax_error_scores_mapping[bracket]
        else:
            stack.append(bracket)

    autocomplete_score = 0
    for bracket in reversed(stack):
        autocomplete_score *= 5
        autocomplete_score += autocomplete_scores_mapping[_brackets_mapping[bracket]]
    return autocomplete_score
