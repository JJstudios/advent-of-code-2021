from typing import Tuple
from functools import cache
from pathlib import Path


class DeterministicDie:
    def __init__(self) -> None:
        self._current_roll = 0
        self._count_rolls = 0

    def roll(self) -> int:
        self._count_rolls += 1
        if self._current_roll == 100:
            self._current_roll = 1
        else:
            self._current_roll += 1
        return self._current_roll

    @property
    def count_rolls(self) -> int:
        return self._count_rolls


class Solver:
    def __init__(self, filename: str):
        self._input_player1_start, self._input_player2_start = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> Tuple[int, int]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()

        input_player1_start = int(input_lines[0].split()[-1])
        input_player2_start = int(input_lines[1].split()[-1])
        return input_player1_start, input_player2_start

    def part_one(self) -> int:
        players_position = [self._input_player1_start - 1, self._input_player2_start - 1]  # Zero-indexed
        players_score = [0, 0]

        die = DeterministicDie()

        player = 0
        while True:
            roll = die.roll() + die.roll() + die.roll()
            players_position[player] = (players_position[player] + roll) % 10
            players_score[player] += players_position[player] + 1
            if players_score[player] >= 1000:
                return players_score[1 - player] * die.count_rolls
            player = 1 - player

    def part_two(self) -> int:
        player1_wins, player2_wins = _run_dirac_dice(
            0, self._input_player1_start - 1, self._input_player2_start - 1, 0, 0
        )
        return max(player1_wins, player2_wins)


@cache
def _run_dirac_dice(
    player: int,
    player1_position: int,
    player2_position: int,
    player1_score: int,
    player2_score: int,
) -> Tuple[int, int]:

    players_wins = [0, 0]
    for roll, count in _dirac_dice_rolls:
        players_position = [player1_position, player2_position]
        players_position[player] = (players_position[player] + roll) % 10
        players_score = [player1_score, player2_score]
        players_score[player] += players_position[player] + 1
        if players_score[player] >= 21:
            players_wins[player] += count
        else:
            player1_subwins, player2_subwins = _run_dirac_dice(
                1 - player, players_position[0], players_position[1], players_score[0], players_score[1]
            )
            players_wins[0] += count * player1_subwins
            players_wins[1] += count * player2_subwins

    return players_wins[0], players_wins[1]


_dirac_dice_rolls = [
    (3, 1),
    (4, 3),
    (5, 6),
    (6, 7),
    (7, 6),
    (8, 3),
    (9, 1),
]
