from typing import List, Optional, Tuple
from enum import Enum, auto
from itertools import count
from pathlib import Path


class SeaCucumber(Enum):
    RIGHT = auto()
    DOWN = auto()


SeaFloor = List[List[Optional[SeaCucumber]]]


class Solver:
    def __init__(self, filename: str):
        self._input_seafloor = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> SeaFloor:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()

        input_seafloor = [list(map(_build_sea_cucumber, line)) for line in input_lines]
        return input_seafloor

    def part_one(self) -> int:
        seafloor = self._input_seafloor
        for step in count(1):
            seafloor, move_count = _move_sea_cucumbers(seafloor)
            if move_count == 0:
                return step
        raise RuntimeError("Unreachable")

    def part_two(self) -> str:
        return "n/a"


def _build_sea_cucumber(symbol: str) -> Optional[SeaCucumber]:
    match symbol:
        case ">":
            return SeaCucumber.RIGHT
        case "v":
            return SeaCucumber.DOWN
        case _:
            return None


def _move_sea_cucumbers(seafloor: SeaFloor) -> Tuple[SeaFloor, int]:
    seafloor, move_count_right = _move_sea_cucumbers_right(seafloor)
    seafloor, move_count_down = _move_sea_cucumbers_down(seafloor)
    return seafloor, move_count_right + move_count_down


def _move_sea_cucumbers_right(seafloor: SeaFloor) -> Tuple[SeaFloor, int]:
    num_rows = len(seafloor)
    num_cols = len(seafloor[0])
    new_seafloor: SeaFloor = [[None for c in range(num_cols)] for r in range(num_rows)]

    move_count = 0
    for c in range(num_cols):
        new_c = (c + 1) % num_cols
        for r in range(num_rows):
            match seafloor[r][c]:
                case SeaCucumber.RIGHT:
                    if not seafloor[r][new_c]:
                        new_seafloor[r][new_c] = SeaCucumber.RIGHT
                        move_count += 1
                    else:
                        new_seafloor[r][c] = SeaCucumber.RIGHT
                case SeaCucumber.DOWN:
                    new_seafloor[r][c] = SeaCucumber.DOWN
    return new_seafloor, move_count


def _move_sea_cucumbers_down(seafloor: SeaFloor) -> Tuple[SeaFloor, int]:
    num_rows = len(seafloor)
    num_cols = len(seafloor[0])
    new_seafloor: SeaFloor = [[None for c in range(num_cols)] for r in range(num_rows)]

    move_count = 0
    for r in range(num_rows):
        new_r = (r + 1) % num_rows
        for c in range(num_cols):
            match seafloor[r][c]:
                case SeaCucumber.RIGHT:
                    new_seafloor[r][c] = SeaCucumber.RIGHT
                case SeaCucumber.DOWN:
                    if not seafloor[new_r][c]:
                        new_seafloor[new_r][c] = SeaCucumber.DOWN
                        move_count += 1
                    else:
                        new_seafloor[r][c] = SeaCucumber.DOWN
    return new_seafloor, move_count
