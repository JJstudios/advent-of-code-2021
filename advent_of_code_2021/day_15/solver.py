from typing import List, Optional
import heapq
from pathlib import Path


class Solver:
    def __init__(self, filename: str):
        self._input_risk_levels = Solver._parse_file(filename)

    @staticmethod
    def _parse_file(filename: str) -> List[List[int]]:
        input_path = Path(__file__).with_name(filename)
        input_text = input_path.read_text(encoding="utf-8")
        input_lines = input_text.splitlines()
        return [list(map(int, line)) for line in input_lines]

    def part_one(self) -> int:
        size = len(self._input_risk_levels)
        costs = [[None for c in range(size)] for r in range(size)]  # type: List[List[Optional[int]]]

        node_queue = [(0, (0, 0))]  # Priority queue with: cost, (r, c)

        while True:
            cost, (r, c) = heapq.heappop(node_queue)
            if r == c == size - 1:
                return cost

            # Check we are not revisiting a node
            if costs[r][c] is not None:
                continue

            costs[r][c] = cost
            for nr, nc in [(r - 1, c), (r, c + 1), (r + 1, c), (r, c - 1)]:
                if 0 <= nr < size and 0 <= nc < size and costs[nr][nc] is None:
                    new_cost = self._input_risk_levels[nr][nc]
                    heapq.heappush(node_queue, (cost + new_cost, (nr, nc)))

    def part_two(self) -> int:
        original_size = len(self._input_risk_levels)
        size = original_size * 5
        costs = [[None for c in range(size)] for r in range(size)]  # type: List[List[Optional[int]]]

        node_queue = [(0, (0, 0))]  # Priority queue with: cost, (r, c)

        while True:
            cost, (r, c) = heapq.heappop(node_queue)
            if r == c == size - 1:
                return cost

            # Check we are not revisiting a node
            if costs[r][c] is not None:
                continue

            costs[r][c] = cost
            for nr, nc in [(r - 1, c), (r, c + 1), (r + 1, c), (r, c - 1)]:
                if 0 <= nr < size and 0 <= nc < size and costs[nr][nc] is None:
                    new_cost = self._input_risk_levels[nr % original_size][nc % original_size]
                    new_cost += nr // original_size + nc // original_size
                    if new_cost > 9:
                        new_cost -= 9
                    heapq.heappush(node_queue, (cost + new_cost, (nr, nc)))
