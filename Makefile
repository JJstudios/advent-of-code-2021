$(if $(shell command -v poetry 2>/dev/null),,$(error "Poetry could not be found. See https://python-poetry.org/docs/"))
$(if $(shell poetry check -q && echo OK),,$(error "pyproject.toml is invalid. Run `poetry check` for more info."))

MODULE := $(shell poetry version --no-ansi | cut -d ' ' -f 1 | tr '-' '_')
INSTALL_MARKER := .poetryinstalled.marker

.PHONY: help
help:  ## Help
	$(info Available targets:)
	@awk '/^[a-z]+:/ { \
		helpCommand = substr($$0, 0, index($$0, ":")-1); \
		helpMessage = ""; \
		if (match($$0, /## (.*)/)) { \
			helpMessage = substr($$0, RSTART+3, RLENGTH-3); \
		} \
		printf "\033[1;32m%-15s\033[0m %s\n", helpCommand, helpMessage; \
	}' $(MAKEFILE_LIST)

.PHONY: install
install: $(INSTALL_MARKER)  ## Install the poetry environment
$(INSTALL_MARKER): pyproject.toml poetry.lock
	poetry install
	touch $(INSTALL_MARKER)

.PHONY: clean
clean:  ## Cleanup build/test artefacts
	find . -type d -name "__pycache__" | xargs rm -rf {};
	rm -rf $(INSTALL_MARKER) .mypy_cache .pytest_cache

.PHONY: lint
lint: $(INSTALL_MARKER)  ## Run lint checks on all source files
	poetry run isort --check $(MODULE)
	poetry run black --check $(MODULE)
	poetry run flake8 $(MODULE)
	poetry run mypy $(MODULE)

.PHONY: format
format: $(INSTALL_MARKER)  ## Format source files
	poetry run isort $(MODULE)
	poetry run black $(MODULE)

.PHONY: test
test: $(INSTALL_MARKER)  ## Run unit tests
	poetry run pytest $(MODULE)
