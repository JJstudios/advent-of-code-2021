Advent of Code 2021
===================

|badge-black| |badge-isort| |badge-mypy|

https://adventofcode.com/2021

.. |badge-black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/psf/black

.. |badge-isort| image:: https://img.shields.io/badge/imports-isort-1674b1?labelColor=ef8336
    :target: https://pycqa.github.io/isort/

.. |badge-mypy| image:: http://www.mypy-lang.org/static/mypy_badge.svg
    :target: http://mypy-lang.org/
